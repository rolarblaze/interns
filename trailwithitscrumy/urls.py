from django.urls import path
from trailwithitscrumy import views
urlpatterns =[
    path('', views.index),
    path('movegoal/<int:goal_id>', views.move_goal),
    path('addgoal', views.add_goal, name="addgoal"),
    path('home', views.home, name="home"),
    
    
]
