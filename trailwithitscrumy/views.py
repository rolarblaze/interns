from django.shortcuts import render
from django.http import HttpResponse
import random
from django.contrib.auth.models import User
from .models import ScrumyGoals, GoalStatus
from django.http import Http404


def index(request):
    s = ScrumyGoals.objects.filter(goal_name='Learn Django') 
    return HttpResponse(s)

def move_goal(request, goal_id):
    #obj = None
    try:
        obj = ScrumyGoals.objects.get(goal_id=goal_id)
    except Exception as e: 
        return render(request, 'trailwithitscrumy/exception.html', {'error': 'A record with that goal id does not exist!'})
    else:
        return HttpResponse(obj.goal_name)   


def add_goal(request):
    #a = GoalStatus.objects.get(status_name="Weekly Goal")
    #user = User.objects.get(username='Louis Oma')

    dict_check =[]
    user_goal = ScrumyGoals.objects.all()

    #looping through
    for m in user_goal:

        #append to the unique list the integer of goal_id of each instance of scrumygoals
        dict_check.append(int(m.goal_id))

        #run a check to test if a number is already generated. then generate a new number 
        while True:

            #generate a random number between 1000 to 9999
            numbers = random.randint(1000, 9999)

            #checking to see if number is unique
            if numbers in dict_check:

                # if a number is in dict_check loop, start generating again.
                continue

                #if number is not in the dict_check 
            else:
                goal_new = GoalStatus.objects.create(status_name="Weekly Goal")
                new_user=User.objects.get(username='Louis Oma')

                scrum_goal = ScrumyGoals(
                goal_name="Keep Learning Django", 
                goal_id=numbers,
                created_by = "Louis", 
                moved_by="Louis", 
                owner="Louis", 
                goal_status=goal_new,
                user=new_user,
                )
                scrum_goal.save()
                break
        return HttpResponse("Keep Learning Django will be created")

#def home(request):
    #send = ScrumyGoals.objects.filter(goal_name="Keep Learning Django")
    #output = ', '.join([n.goal_name for n in send])
   # return HttpResponse(output) 

def home(request):
    #goal_n = None, try:except ScrumyGoals.DoesNotExist: return HttpResponse('Goals does not exist')
    
    goal = ScrumyGoals.objects.get(goal_name='Learn Django')
    
       
    dictionary = {
        'goal_name': goal.goal_name,        
        'goal_id':  goal.goal_id,                    
        'user':     goal.user,                           
    }
    return render(request, 'trailwithitscrumy/home.html', dictionary)



    #goal_n.goal_name,        #ScrumyGoals.objects.get(goal_name='Learn Django'),
     #ScrumyGoals.objects.get(goal_id=1),
     #User.objects.get(username='Louis Oma'),
