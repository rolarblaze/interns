from django.db import models
from django.contrib.auth.models import User
from django.utils import timezone

# Create your models here.
class GoalStatus(models.Model):
    
    status_name = models.CharField(max_length=250, default='pending')

    def __str__(self):
        return self.status_name

class ScrumyGoals(models.Model):
    goal_name = models.CharField(max_length=200)
    goal_id = models.IntegerField(default=1)
    created_by = models.CharField(max_length=200)
    moved_by = models.CharField(max_length=200)
    owner = models.CharField(max_length=250)
    user = models.ForeignKey(User, related_name='goal_created', on_delete=models.PROTECT)
    goal_status = models.ForeignKey(GoalStatus, on_delete=models.PROTECT)

    #objects = models manager()
    def __str__(self):
        return self.goal_name

class ScrumyHistory(models.Model):
    moved_by = models.CharField(max_length=250)
    created_by = models.CharField(max_length=250)
    moved_from = models.CharField(max_length=100)
    moved_to = models.CharField(max_length=100)
    goal = models.ForeignKey(ScrumyGoals, on_delete=models.CASCADE)

    def __str__(self):
        return self.created_by