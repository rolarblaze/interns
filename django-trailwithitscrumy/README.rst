=====
trailwithitscrumy Apps
=====

trailwithitscrumy is a Django app to conduct Web-based polls. For each question,
visitors can choose between a fixed number of answers.

Detailed documentation is in the "docs" directory.

Quick start
-----------

1. Add "trailwithitscrumy" to your INSTALLED_APPS setting like this::

    INSTALLED_APPS = [
        ...
        'trailwithitscrumy.apps.trailwithitscrumyConfig',
    ]

2. Include the polls URLconf in your project urls.py like this::

    path('trailwithitscrumy/', include('trailwithitscrumy.urls')),

3. Run ``python manage.py migrate`` to create the trailwithitscrumy models.

4. Start the development server and visit http://127.0.0.1:8000/admin/
   to create a trailwithitscrumy (you'll need the Admin app enabled).

5. Visit http://3.91.203.148:8000/trailwithitscrumy/ to participate in the poll.